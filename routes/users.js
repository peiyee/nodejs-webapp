var express = require('express');
var router = express.Router();
var db = require('../models')

/* GET users listing. */
router.get('/', function (req, res, next) {
  return db.User.findAll()
    .then(users => {
      res.render('users/index', { users: users });
    })
});

router.get('/:id', function (req, res, next) {
  console.log(req.params)
  return db.User.findByPk(req.params.id)
    .then(user => {
      res.render('users/show', { user: user });
    })
});

module.exports = router;
